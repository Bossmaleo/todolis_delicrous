import 'package:flutter/material.dart';
import 'package:todolist/classes/Choice.dart';
import 'package:badges/badges.dart';
import 'package:todolist/classes/ChoiceCardList.dart';
import 'package:todolist/PanierList.dart';
import 'package:todolist/detailsfoodmenu.dart';

class GridPage extends StatefulWidget  {
  @override
  GridPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _GridPageState createState() => _GridPageState();
}

class _GridPageState extends State<GridPage> {

  String monVal = '2';

  var nameAndrize = new Row(
    children: <Widget>[
      new Padding(
        padding: const EdgeInsets.only(right:10.0),
        child: Text("Lorem ipsum",
        ),
      ),
      new Padding(
        padding: const EdgeInsets.only(right:50.0),
        child: Text("10 €",
        ),
      ),
    ],
  );
  
  var imageboss = new Padding(
      padding: const EdgeInsets.all(5.0),
      child:Image.asset('icons/image1.jpg'),
  );

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => PanierList()));
              },
              child: Padding(
                padding: const EdgeInsets.only(right:15.0,top: 20.0),
                child:Badge(
                  badgeContent: Text("2",
                  ),
                  child: Icon(Icons.shopping_cart,color: Colors.black),

                ),
              ),
            ),
          ]
        ),
        body: GridView.custom(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
          ),

          // Generate 100 widgets that display their index in the List.
          childrenDelegate: SliverChildListDelegate (List.generate(choicecardlist.length, (index) {
            return GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => detailsfoodmenu()));
              },
              child:Card(
              child: Container(
              child: Column(
                children: [
                  Image.asset(choicecardlist[index].imageplat),
                  Row(
                    children: <Widget>[
                      new Padding(
                        padding: const EdgeInsets.only(left:5.0),
                      child: Text(choicecardlist[index].nomplat),
                      ),
                     new Padding(
                       padding: const EdgeInsets.only(left:10.0),
                       child: Text(choicecardlist[index].prixplat,
                           style: TextStyle(color: Colors.deepOrange[100])),
                     )
                    ],
                  ),

                  Row(
                    children: <Widget>[
                      new Padding(
                        padding: const EdgeInsets.only(left:5.0,top:0),
                        child: Text(choicecardlist[index].detailsbotom,
                        style: TextStyle(fontSize: 7.0),),
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(top:11.0,left:48.0,bottom: 0),
                        child: Checkbox(
                          activeColor: Colors.red,
                          value: choicecardlist[index].checkboxvalue,
                          onChanged: (bool value){
                            setState(() {
                              choicecardlist[index].checkboxvalue = value;
                              if(value){
                                //Navigator.push(context, MaterialPageRoute(builder: (context) => detailsfoodmenu()));
                              }
                            });
                          },
                        ),
                      )
                    ],
                  ),

                ],
              ),

            ),),
            );
          }),
        ),
        ),
      );
  }



  static const List<Choice> choices = const <Choice>[
    const Choice(title: 'Walk', icon: Icons.shopping_cart),
  ];

   List<ChoiceCardList> choicecardlist =  <ChoiceCardList>[
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€' ,'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ', ' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
       ChoiceCardList( 'Lorem Ipsum dolor ',' 10€', 'icons/image1.jpg',"Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",false),
  ];
}

