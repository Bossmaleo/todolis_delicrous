import 'package:flutter/material.dart';
import 'package:badges/badges.dart';

class PanierList extends StatelessWidget {

  final ContenuPanier = [
    'Maleo-sama',
    'Regis-sama',
    'Sidney-sama',
    'Maleo-sama',
    'Regis-sama',
    'Sidney-sama',
    'Maleo-sama',
    'Regis-sama',
    'Sidney-sama',
    'Maleo-sama',
    'Regis-sama',
    'Sidney-sama',
  ];


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar:AppBar(
          title: Text("Delivecrous"),
          actions: <Widget>[
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => PanierList()));
              },
              child: Padding(
                padding: const EdgeInsets.only(right:15.0,top: 20.0),
                child:Badge(
                  badgeContent: Text("2",
                  ),
                  child: Icon(Icons.shopping_cart,color: Colors.black),

                ),
              ),
            ),
          ]
      ),
      body:  Column(
        children: <Widget>[
          SizedBox(
            height: 300,
            child:  ListView.builder(
              itemCount: ContenuPanier.length,
              itemBuilder: (context,index){
                return new Card(
                    child:Column(
                      children: <Widget>[
                        Row(
                            children: <Widget>[
                              Image.asset(
                                "icons/image1.jpg",
                                width:100.0,
                              ),
                              Column(
                                  children: <Widget>[
                                    Row(
                                        children: <Widget>[
                                          new Padding(
                                            padding: const EdgeInsets.only(right:0.0,left:0.0),
                                            child: Text("Lorem Ipsum dolor",
                                              style: TextStyle(fontSize: 15.0),),
                                          ),
                                          new Padding(
                                            padding: const EdgeInsets.only(left:50.0),
                                            child: Text("10€",
                                                style: TextStyle(color: Colors.deepOrange[100])),
                                          )
                                        ]
                                    ),
                                    Row(
                                        children: <Widget>[
                                          new Padding(
                                            padding: const EdgeInsets.only(left:15.0),
                                            child: Text("Lorem ipsum dolor sit \n amat, consectetur \nadipiscing elit",
                                              style: TextStyle(fontSize: 10.0),),
                                          ),
                                          new Padding(
                                            padding: const EdgeInsets.only(left:70.0),
                                            child: Checkbox(
                                              activeColor: Colors.red,
                                              value:true,
                                              onChanged: (bool value){

                                              },
                                            ),
                                          )

                                        ]
                                    )
                                  ]
                              )
                            ]
                        )
                      ],
                    )
                );


              },),
          ),
          Row(
            children: <Widget>[
              Text(""),
              Text("Total 20€")
            ],
          ),
          Text("Où veux-tu te faire livrer? "
              "\n En salle de TD?"),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Rue'
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Ville'),
                ),
              ),
              Expanded(
                flex: 4,
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Code Postal'),
                ),
              ),
            ],
          ),
          FlatButton(
            onPressed: () {
              /*...*/
            },
            child: Text(
              "Passer commande",
            ),
          )
        ],
      )
    );

  }
}